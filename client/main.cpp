#include <iostream>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

enum {max_length = 1024};

std::string next_chunk(tcp::socket& s)
{
        std::string str;
        char reply[max_length + 1];
        boost::asio::read(s, boost::asio::buffer(reply, max_length));
        auto reply_len = atoi(reply);
        if (reply_len == -1)
            return str;
        boost::asio::read(s, boost::asio::buffer(reply, max_length));
        reply[reply_len] = '\0';
        str = std::string(reply, reply + reply_len);
        return str;
}

int main(int argc, char* argv[])
{

    if (argc != 4)
    {
        std::cerr << "Usage: client <host> <port> <filename>\n";
        return 1;
    }

    try
    {
        boost::asio::io_context io_context;

        tcp::socket s(io_context);
        tcp::resolver resolver(io_context);
        boost::asio::connect(s, resolver.resolve(argv[1], argv[2]));

        size_t request_length = std::strlen(argv[3]);
        boost::asio::write(s, boost::asio::buffer(argv[3], request_length));

        std::ofstream file(argv[3], std::ofstream::binary);
        auto str = next_chunk(s);
        if (str.empty())
        {
            std::cerr << "File not found\n";
            return -1;
        }
        while (!str.empty())
        {
            file.write(str.c_str(), str.length());;
            str = next_chunk(s);
        }

        file.close();
    } catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
