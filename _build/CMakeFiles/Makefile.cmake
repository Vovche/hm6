# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.14

# The generator used is:
set(CMAKE_DEPENDS_GENERATOR "Unix Makefiles")

# The top level Makefile was generated from the following files:
set(CMAKE_MAKEFILE_DEPENDS
  "CMakeCache.txt"
  "../CMakeLists.txt"
  "CMakeFiles/3.14.0/CMakeCCompiler.cmake"
  "CMakeFiles/3.14.0/CMakeCXXCompiler.cmake"
  "CMakeFiles/3.14.0/CMakeSystem.cmake"
  "../client/CMakeLists.txt"
  "../server/CMakeLists.txt"
  "../server/lib/CMakeLists.txt"
  "../server/tests/CMakeLists.txt"
  "/usr/share/cmake-3.14/Modules/CMakeCInformation.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeCXXInformation.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeCheckCompilerFlagCommonPatterns.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeCommonLanguageInclude.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeGenericSystem.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeInitializeConfigs.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeLanguageInformation.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeSystemSpecificInformation.cmake"
  "/usr/share/cmake-3.14/Modules/CMakeSystemSpecificInitialize.cmake"
  "/usr/share/cmake-3.14/Modules/CheckIncludeFile.cmake"
  "/usr/share/cmake-3.14/Modules/CheckLibraryExists.cmake"
  "/usr/share/cmake-3.14/Modules/CheckSymbolExists.cmake"
  "/usr/share/cmake-3.14/Modules/Compiler/CMakeCommonCompilerMacros.cmake"
  "/usr/share/cmake-3.14/Modules/Compiler/GNU-C.cmake"
  "/usr/share/cmake-3.14/Modules/Compiler/GNU-CXX.cmake"
  "/usr/share/cmake-3.14/Modules/Compiler/GNU.cmake"
  "/usr/share/cmake-3.14/Modules/FindBoost.cmake"
  "/usr/share/cmake-3.14/Modules/FindGTest.cmake"
  "/usr/share/cmake-3.14/Modules/FindPackageHandleStandardArgs.cmake"
  "/usr/share/cmake-3.14/Modules/FindPackageMessage.cmake"
  "/usr/share/cmake-3.14/Modules/FindThreads.cmake"
  "/usr/share/cmake-3.14/Modules/GoogleTest.cmake"
  "/usr/share/cmake-3.14/Modules/Internal/CMakeCheckCompilerFlag.cmake"
  "/usr/share/cmake-3.14/Modules/Platform/Linux-GNU-C.cmake"
  "/usr/share/cmake-3.14/Modules/Platform/Linux-GNU-CXX.cmake"
  "/usr/share/cmake-3.14/Modules/Platform/Linux-GNU.cmake"
  "/usr/share/cmake-3.14/Modules/Platform/Linux.cmake"
  "/usr/share/cmake-3.14/Modules/Platform/UnixPaths.cmake"
  )

# The corresponding makefile is:
set(CMAKE_MAKEFILE_OUTPUTS
  "Makefile"
  "CMakeFiles/cmake.check_cache"
  )

# Byproducts of CMake generate step:
set(CMAKE_MAKEFILE_PRODUCTS
  "CMakeFiles/CMakeDirectoryInformation.cmake"
  "server/CMakeFiles/CMakeDirectoryInformation.cmake"
  "server/lib/CMakeFiles/CMakeDirectoryInformation.cmake"
  "server/tests/CMakeFiles/CMakeDirectoryInformation.cmake"
  "client/CMakeFiles/CMakeDirectoryInformation.cmake"
  )

# Dependency information for all targets:
set(CMAKE_DEPEND_INFO_FILES
  "server/CMakeFiles/server.dir/DependInfo.cmake"
  "server/lib/CMakeFiles/libserver.dir/DependInfo.cmake"
  "server/tests/CMakeFiles/libservertest.dir/DependInfo.cmake"
  "client/CMakeFiles/client.dir/DependInfo.cmake"
  )
