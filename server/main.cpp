#include "server.h"
#include <iostream>

int main (int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: async_tcp_echo_server <port>\n";
        return 1;
    }
    short port = std::atoi(argv[1]);
    try
    {

        boost::asio::io_context io_context;

        Server s(io_context, port);

        io_context.run();
    } catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }
    return 0;
}
