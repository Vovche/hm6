#include <gtest/gtest.h>
#include "server.h"
#include <iostream>

TEST(FileParserTest, SimpleTest)
{
    FileParser parser("testfile", 5);
    ASSERT_TRUE(std::string("Hello") == parser.next());
    ASSERT_TRUE(std::string(" hell") == parser.next());
    ASSERT_TRUE(std::string("o\n") == parser.next());
}
