#include "file_parser.h"
#include <iostream>

FileParser::FileParser(const std::string& file_name, std::size_t size) :
    chunk_size_(size)
{
    std::ifstream file(file_name, std::ifstream::binary); 
    if (!file)
    {
        It = chunks_.end();
        return;
    }
    // get file length
    file.seekg (0, file.end);
    auto length = file.tellg();
#ifdef DEBUG
    std::cout << "File length: " << length << std::endl;
#endif
    file.seekg (0, file.beg);
    if (length == 0)
        return;
    int chunks_num = length / chunk_size_;
#ifdef DEBUG
    std::cout << "Chunks num: " << chunks_num << std::endl;
#endif
    auto buff = new char[chunk_size_ + 1];
    int i = 0;
    while (i < chunks_num)
    {
        file.read(buff, chunk_size_);
        buff[chunk_size_] = '\0';
        std::string str(buff, buff + chunk_size_);
#ifdef DEBUG
        std::cout << "Buff size is " << str.length() << std::endl;
#endif
        chunks_.push_back(str);
        file.seekg((++i) * chunk_size_);
    }
    if (static_cast<decltype(length)>
        (chunk_size_ * chunks_num) != length)
    {
        length =  length -
            static_cast<decltype(length)>
                (chunk_size_ * chunks_num);
        file.read(buff, length);

        buff[length] = '\0';
        std::string str(buff, buff + length);
        chunks_.push_back(str);
    }
    It = chunks_.begin();
    delete[] buff;
    file.close();
}

std::string FileParser::next()
{
    return *It++;
}

bool FileParser::end()
{
    return It == chunks_.end();
}

