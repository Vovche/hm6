#ifndef FILE_PARSER_H
#define FILE_PARSER_H

#include <fstream>
#include <vector>

#define DEBUG
#undef DEBUG

class FileParser
{
    using vtype = std::vector<std::string>;
public:
    FileParser(const std::string& file_name, std::size_t size);
    std::string next();
    bool end();

private:
    std::size_t chunk_size_;
    vtype chunks_;
    vtype::iterator It;
};
#endif
