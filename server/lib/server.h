#ifndef SERVER_H
#define SERVER_H

#include <boost/asio.hpp>
#include <functional>
#include <iostream>
#include <string.h>

#include "file_parser.h"

#define DEBUG

using tcp = boost::asio::ip::tcp;
using io_context = boost::asio::io_context;
using err_code = boost::system::error_code;

class Session : public std::enable_shared_from_this<Session>
{
public:
    Session(tcp::socket&& socket) :
        socket_(std::move(socket)) {}

    void start() { do_read(); }

private:
    void do_read()
    {
        auto self(shared_from_this());
        socket_.async_read_some(
            boost::asio::buffer(buff_, max_length),
            [this, self] (err_code e, std::size_t length)
            {
                if (!e)
                {
                    buff_[length] = '\0';
                    std::string filename(buff_);
#ifdef DEBUG
                    std::cout << "Got filename " << filename << std::endl;
#endif
                    try
                    {
                        parser.reset(new FileParser(filename, max_length));
                        do_write();
                    } catch(std::invalid_argument& e)
                    {
                        return;
                    }
                }
            });
    }

    void do_write()
    {
        if (parser->end())
        {
            auto len_s = std::to_string(-1);
            strcpy(buff_, len_s.c_str());
            buff_[strlen(len_s.c_str())] = '\0';
            boost::asio::async_write(socket_,
                boost::asio::buffer(buff_, max_length),
                [] (err_code, std::size_t) {return;});
#ifdef DEBUG
            std::cout << "End connection" << std::endl;
#endif
            return;
        }
        auto self(shared_from_this());
        auto str = parser->next();
#ifdef DEBUG
//        std::cout << "Sending string:" <<std::endl << str << std::endl;
//        std::cout << "With length: " << str.length() << std::endl;
#endif
        //sending chunk length and chunk then
        auto len_s = std::to_string(str.length());
        strcpy(buff_, len_s.c_str());
        buff_[strlen(len_s.c_str())] = '\0';
        boost::asio::async_write(socket_,
            boost::asio::buffer(buff_, max_length),
            [str = str, this, self] (err_code e, std::size_t /*length*/)
            {
                if (!e)
                {
                    boost::asio::async_write(socket_,
                        boost::asio::buffer(str.c_str(), max_length),
                        [this, self] (err_code e, std::size_t /*length*/)
                        {
                            if (!e)
                            {
                                do_write();
                            }
                        });
                }
            });
    }

    tcp::socket socket_;
    static const std::size_t max_length{1024};
    char buff_[max_length + 1];
    std::unique_ptr<FileParser> parser{nullptr};
};

class Server
{
public:
    Server(io_context& io_context, short port)
    : acceptor_(io_context, tcp::endpoint(tcp::v4(), port))
    {
        do_accept();
    }

private:
    void do_accept()
    {
        acceptor_.async_accept(
        [this](err_code e, tcp::socket socket)
        {
            if (!e)
            {
                std::make_shared<Session>(std::move(socket))->start();
            }

            do_accept();
        });
    }
   
private:
     tcp::acceptor acceptor_;
};

#endif
