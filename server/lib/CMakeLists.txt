add_library(libserver STATIC server.h file_parser.h
    file_parser.cpp
)
target_link_libraries(libserver ${Boost_LIBRARIES})
