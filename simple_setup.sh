#!/bin/bash
rm -rf temp
mkdir temp
mkdir temp/server
mkdir temp/client

cp -f _build/server/server temp/server/server
cp -f _build/client/client temp/client/client
