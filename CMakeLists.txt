cmake_minimum_required(VERSION 3.12)
set(HOMEWORK_NUMBER 6)
set(NUMBER_OF_TASKS 1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-g -Wall -Wextra -Werror -pthread" ) 

project(HOMEWORK${HOMEWORK_NUMBER})

enable_testing()
find_package(GTest REQUIRED)
find_package(Boost 1.40.0 REQUIRED COMPONENTS system)
include_directories(${GTEST_INCLUDE_DIR})
include_directories(${Boost_INCLUDE_DIR})

add_subdirectory(server)
add_subdirectory(client)
