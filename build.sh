#!/bin/bash

BUILD_DIR=_build

if [ ! -d $BUILD_DIR ];
then
    mkdir $BUILD_DIR
fi;
cd $BUILD_DIR
cmake ..
make
echo "Hello hello" > server/tests/testfile
ctest -V
cp -f server/server ../temp/server
cp -f client/client ../temp/client
